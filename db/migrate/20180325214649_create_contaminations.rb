class CreateContaminations < ActiveRecord::Migration[5.0]
  def change
    create_table :contaminations do |t|
      t.references :survivor, foreign_key: true
      t.references :contamined_survivor, foreign_key: true

      t.timestamps
    end
    add_index :contaminations, [:survivor_id, :contamined_survivor_id], unique: true
  end
end
