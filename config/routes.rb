Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :survivors, only: [:create, :update]
      resources :contaminations, only: :create
      resources :trades, only: :create
      resources :reports, only: [] do
        collection do
          get 'infected-survivor-percentage'
          get 'non-infected-survivor-percentage'
          get 'infected-points-lost'
        end
      end
      get "/reports/average-item-by-survivor/:item_id", to: 'reports#average_item_by_survivor'
    end
  end
end
