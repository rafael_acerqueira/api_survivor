require 'rails_helper'

describe CalculateTradePointsService do
  context ".call" do
    it "Calculate inventory poins" do
      item_one = create(:item, points: 2)
      item_two = create(:item, points: 3)
      inventory_one = create(:inventory, quantity: 1, item: item_one)
      inventory_two = create(:inventory, quantity: 2, item: item_two)
      inventories = []
      inventories.push({ quantity: 1, inventory_id: inventory_one.id })
      inventories.push({ quantity: 1, inventory_id: inventory_two.id })
      service = CalculateTradePointsService.new(inventories)
      expect(service.call).to eq(5)
    end
  end
end
