require 'rails_helper'

describe CheckContaminationService do
  context ".call" do
    it "Check whether survivor is contaminated" do
      create_list(:item, 5)
      survivor_two = create(:survivor, name: 'Really Sick')
      create_list(:contamination, 3, contamined_survivor: survivor_two)

      inventory_one = survivor_two.inventories.first
      inventory_two = survivor_two.inventories.last
      inventories = []
      inventories.push({ quantity: 1, inventory_id: inventory_one.id })
      inventories.push({ quantity: 1, inventory_id: inventory_two.id })
      service = CheckContaminationService.new(inventories)      
      expect(service.call).to be_truthy
    end

    it "Check whether survivor isnt contaminated" do
      create_list(:item, 5)
      survivor_two = create(:survivor, name: 'Really Sick')
      create_list(:contamination, 2, contamined_survivor: survivor_two)

      inventory_one = survivor_two.inventories.first
      inventory_two = survivor_two.inventories.last
      inventories = []
      inventories.push({ quantity: 1, inventory_id: inventory_one.id })
      inventories.push({ quantity: 1, inventory_id: inventory_two.id })
      service = CheckContaminationService.new(inventories)
      expect(service.call).to be_falsey
    end
  end
end
