require 'rails_helper'

describe TradeItemsService do
  context ".call" do
    it "Trade items between two survivors" do
      item_one = create(:item, points: 2)
      item_two = create(:item, points: 3)
      item_three = create(:item, points: 1)

      survivor_one = create(:survivor)
      inventory_one = create(:inventory, quantity: 5, item: item_one, survivor: survivor_one)
      inventory_two = create(:inventory, quantity: 3, item: item_two, survivor: survivor_one)
      inventory_list_one = []
      inventory_list_one.push({ quantity: 3, inventory_id: inventory_one.id })
      inventory_list_one.push({ quantity: 1, inventory_id: inventory_two.id })

      survivor_two = create(:survivor)
      inventory_three = create(:inventory, quantity: 8, item: item_one, survivor: survivor_two)
      inventory_four = create(:inventory, quantity: 5, item: item_three, survivor: survivor_two)
      inventory_five = create(:inventory, quantity: 6, item: item_two, survivor: survivor_two)
      inventory_list_two = []
      inventory_list_two.push({ quantity: 1, inventory_id: inventory_three.id })
      inventory_list_two.push({ quantity: 1, inventory_id: inventory_four.id })
      inventory_list_two.push({ quantity: 2, inventory_id: inventory_five.id })

      service = TradeItemsService.new(inventory_list_one, inventory_list_two)
      service.call

      expect(survivor_one.inventories.find_by_id(inventory_one).quantity).to eq(3)
      expect(survivor_one.inventories.find_by_id(inventory_two).quantity).to eq(4)

      expect(survivor_two.inventories.find_by_id(inventory_three).quantity).to eq(10)
      expect(survivor_two.inventories.find_by_id(inventory_five).quantity).to eq(5)
    end
  end
end
