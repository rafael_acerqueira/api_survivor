require 'rails_helper'

RSpec.describe "Api::V1::Survivors", type: :request do

  describe "POST /survivors" do
    before{ create_list(:item, 4) }
    context 'when the request is valid' do
      before do
        @survivor_attributes = attributes_for(:survivor)
        post '/api/v1/survivors', params: { survivor: @survivor_attributes }
      end

      it 'creates a survivor and related items' do
        created_survivor = Survivor.first
        @survivor_attributes.each do |key, value|
          expect(created_survivor[key]).to eq(value)
        end
        expect(created_survivor.items).to_not be_empty
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before do
        post '/api/v1/survivors', params: { survivor: { name: 'Only name' } }
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Age can't be blank, Gender can't be blank, Latitude can't be blank, Longitude can't be blank/)
      end
    end
  end

  describe "PUT /survivor/:id" do
    before do
      create_list(:item, 4)
      @survivor = create(:survivor)
    end

    context "When survivor exists" do
      context "And update laitude and longitude fields" do
        before do
          @survivor_attributes = attributes_for(:survivor).slice(:latitude, :longitude)
          put "/api/v1/survivors/#{@survivor.id}", params: {survivor: @survivor_attributes}
        end

        it "returns status code 204" do
          expect(response).to have_http_status(204)
        end

        it "survivor are updated with correct data" do
          @survivor.reload
          @survivor_attributes.each do |key, value|
            expect(@survivor[key]).to eq(value)
          end
        end
      end

      context "And try updating fields that arent latitude and longitude but it cant" do

        before do
          @survivor_attributes = {name: 'José', gender: 'Female', age: 52}
          put "/api/v1/survivors/#{@survivor.id}", params: {survivor: @survivor_attributes}
        end

        it "returns status code 204" do
          expect(response).to have_http_status(204)
        end

        it "survivor arent updated name, age and gender" do
          @survivor.reload
          @survivor_attributes.each do |key, value|
            expect(@survivor[key]).to_not eq(value)
          end
        end
      end
    end

    context "When survivor exists" do
      before do
        @survivor_attributes = attributes_for(:survivor)
        put "/api/v1/survivors/0", params: {survivor: @survivor_attributes}
      end

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
      
      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Survivor/)
      end
    end
  end
end
