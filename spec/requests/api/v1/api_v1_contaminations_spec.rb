require 'rails_helper'

RSpec.describe "Api::V1::Contaminations", type: :request do

  describe "POST /contaminations" do

    context 'when the request is valid' do
      before do
        create_list(:item, 4)
        @survivor = create(:survivor)
        @contamined_survivor = create(:survivor)
        post '/api/v1/contaminations', params: { contamination: {survivor_id: @survivor.id, contamined_survivor_id: @contamined_survivor.id} }
      end

      it 'creates a contamination record' do
        created_contamination = Contamination.first
        expect(created_contamination.survivor). eql?(@survivor)
        expect(created_contamination.contamined_survivor). eql?(@contamined_survivor)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before do
        post '/api/v1/contaminations', params: { contamination: {survivor_id: 0} }
      end

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Survivor must exist, Contamined survivor must exist/)
      end
    end
  end
end
