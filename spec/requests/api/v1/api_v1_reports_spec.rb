require 'rails_helper'

RSpec.describe "Api::V1::Reports", type: :request do

  describe "GET /reports/infected-survivor-percentage" do
    before do
      create(:item)
      survivor_one = create(:survivor)
      create_list(:contamination, 3, contamined_survivor: survivor_one)
      get '/api/v1/reports/infected-survivor-percentage'
    end

    it "return percentage of infected survivor" do
      expect(response.body).to eq("25% of survivors are infected")
    end
  end

  describe "GET /reports/non-infected-survivor-percentage" do
    before do
      create(:item)
      survivor_one = create(:survivor)
      create_list(:contamination, 3, contamined_survivor: survivor_one)
      get '/api/v1/reports/non-infected-survivor-percentage'
    end

    it "return percentage of non-infected survivor" do
      expect(response.body).to eq("75% of survivors are infected")
    end
  end

  describe "GET /reports/average-item-by-survivor/:item_id" do
    before do
      create(:item)

      survivor_three = create(:survivor)
      create_list(:contamination, 3, contamined_survivor: survivor_three)
      survivor_one = create(:survivor)
      survivor_two = create(:survivor)


      @item_one = create(:item, points: 1)
      @item_two = create(:item, points: 2)
      @item_three = create(:item, points: 3)
      @item_four = create(:item, points: 4)

      inventory_one = create(:inventory, survivor_id: survivor_one.id,
         quantity: 5, item_id: @item_one.id)
      inventory_two = create(:inventory, survivor_id: survivor_one.id,
         quantity: 1, item_id: @item_four.id)
      inventory_three = create(:inventory, survivor_id: survivor_one.id,
         quantity: 2, item_id: @item_three.id)

      inventory_four = create(:inventory, survivor_id: survivor_two.id,
         quantity: 5, item_id: @item_one.id)
      inventory_five = create(:inventory, survivor_id: survivor_two.id,
         quantity: 4, item_id: @item_three.id)

      inventory_six = create(:inventory, survivor_id: survivor_three.id,
         quantity: 4, item_id: @item_one.id)


      get "/api/v1/reports/average-item-by-survivor/#{@item_one.id}"
    end

    it "return average amount of each kind of resource by non-infected survivor" do
      expect(response.body).to eq("2 #{@item_one.name} per survivor")
    end
  end

    describe "GET /reports/infected-points-lost" do
      before do
        create(:item)
        @survivor_one = create(:survivor)
        @survivor_two = create(:survivor)
        survivor_three = create(:survivor)
        create_list(:contamination, 3, contamined_survivor: @survivor_one)
        create_list(:contamination, 5, contamined_survivor: @survivor_two)
        create_list(:contamination, 2, contamined_survivor: survivor_three)
        get '/api/v1/reports/infected-points-lost'
      end

      it "return points lost because of infected survivor" do
        expect(response.body).to eq("#{@survivor_one.total_points + @survivor_two.total_points} points were lost")
      end
    end
end
