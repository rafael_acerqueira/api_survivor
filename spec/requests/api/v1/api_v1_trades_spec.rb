require 'rails_helper'

RSpec.describe "Api::V1::Trades", type: :request do

  describe "POST /trades" do
    before do
      @never_used = create(:item, points: 0, name: 'Never Used')

      @survivor_one = create(:survivor)
      @survivor_two = create(:survivor)

      @item_one = create(:item, points: 1)
      @item_two = create(:item, points: 2)
      @item_three = create(:item, points: 3)
      @item_four = create(:item, points: 4)

      @inventory_one = create(:inventory, survivor_id: @survivor_one.id,
         quantity: 4, item_id: @item_one.id)
      @inventory_two = create(:inventory, survivor_id: @survivor_one.id,
         quantity: 1, item_id: @item_four.id)
      @inventory_three = create(:inventory, survivor_id: @survivor_one.id,
         quantity: 2, item_id: @item_three.id)

      @inventory_four = create(:inventory, survivor_id: @survivor_two.id,
         quantity: 5, item_id: @item_two.id)
      @inventory_five = create(:inventory, survivor_id: @survivor_two.id,
         quantity: 4, item_id: @item_three.id)
    end

    context "when the survivors arent infected" do
      context "and they trade with same points quantity" do
        context "and trade one item by other" do
          before do
            post '/api/v1/trades',
            params: {
              trade: {
                inventory_list_survivor_one: [
                  quantity: 2, inventory_id: @inventory_one.id
                ],
                inventory_list_survivor_two: [
                  quantity: 1, inventory_id: @inventory_four.id
                ]
              }
            }
          end

          it "returns status code 204" do
            expect(response).to have_http_status(204)
          end

          it "change quantity in the invetories traded" do
            @inventory_one.reload
            @inventory_four.reload
            expect(@inventory_one.quantity).to eq(2)
            expect(@inventory_four.quantity).to eq(4)
          end

          it "inventories with diffent items create new inventory" do
            expect(@survivor_one.inventories.pluck(:item_id)).to include(@item_two.id)
            expect(@survivor_two.inventories.pluck(:item_id)).to include(@item_one.id)
          end
        end

        context "and trade more than one items by other(s)" do
          before do
            post '/api/v1/trades',
            params:{
              trade:{
                inventory_list_survivor_one: [
                  [quantity: 2, inventory_id: @inventory_one.id ],
                  [ quantity: 1, inventory_id: @inventory_two.id ],
                  [ quantity: 1, inventory_id: @inventory_three.id ]
                ],
                inventory_list_survivor_two: [
                  quantity: 3, inventory_id:@inventory_five.id
                ]
              }
            }
          end

          it "returns status code 204" do
            expect(response).to have_http_status(204)
          end

          it "inventories with same item increase quantity" do
            @inventory_three.reload
            @inventory_five.reload
            expect(@inventory_three.quantity).to eq(4)
            expect(@inventory_five.quantity).to eq(2)
          end

          it "inventories with diffent items create new inventory" do
            expect(@survivor_two.inventories.pluck(:item_id)).to include(@item_one.id, @item_four.id)
          end
        end
      end

      context "and they trade with diffent points quantity" do
        before do
          post '/api/v1/trades',
          params: {
            trade: {
              inventory_list_survivor_one: [
                [ quantity: 3, inventory_id: @inventory_one.id ]
              ],
              inventory_list_survivor_two: [
                [ quantity: 5, inventory_id: @inventory_four.id ]
              ]
            }
          }
        end
        it 'returns status code 404' do
          expect(response).to have_http_status(404)
        end
      end
    end

    context "when one of the survivors is infected" do
      before do
        create_list(:survivor, 3)
        create_list(:contamination, 3, contamined_survivor: @survivor_two)
        
        post '/api/v1/trades',
        params: {
          trade: {
            inventory_list_survivor_one: [
              quantity: 2, inventory_id: @inventory_one.id
            ],
            inventory_list_survivor_two: [
              quantity: 1, inventory_id: @inventory_four.id
            ]
          }
        }
      end

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
    end
  end
end
