FactoryBot.define do
  factory :inventory do
    survivor
    quantity [1, 20, 8].sample
    item
  end
end
