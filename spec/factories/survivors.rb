FactoryBot.define do
  factory :survivor do
    name { FFaker::Name.name }
    age [20, 30, 40, 80].sample
    gender { FFaker::Gender.maybe }
    latitude {FFaker::Geolocation.lat}
    longitude {FFaker::Geolocation.lng}
  end
end
