FactoryBot.define do
  factory :contamination do
    survivor
    association :contamined_survivor, factory: :survivor
  end
end
