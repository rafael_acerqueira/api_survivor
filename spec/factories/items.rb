FactoryBot.define do
  factory :item do
    name { FFaker::Name.name }
    points [1, 2, 3, 4].sample
  end
end
