class Survivor < ApplicationRecord

  CONTAMINED_COUNT = 3

  after_create :create_items

  validates :name, :age, :gender, :latitude, :longitude, presence: true

  attr_readonly :name, :age, :gender

  has_many :items, through: :inventories
  has_many :inventories
  has_many :contaminations, dependent: :destroy

  def create_items
    (1..5).to_a.each do |i|
      item = Item.all.sample
      inventory = inventories.find_by(item: item)
      if inventory.present?
        inventory.update(quantity: inventory.quantity + i)
      else
        Inventory.create(survivor_id: self.id, quantity: i, item_id: item.id)
      end
    end
  end

  def total_points
    inventories.inject(0) {|sum, n| sum + n.total_points }
  end

  def contamined?
    Contamination.where(contamined_survivor: self).count >= CONTAMINED_COUNT    
  end
end
