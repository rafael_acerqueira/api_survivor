class Contamination < ApplicationRecord
  belongs_to :survivor
  belongs_to :contamined_survivor, class_name: 'Survivor'

  validates :survivor, uniqueness: { scope: :contamined_survivor }

end
