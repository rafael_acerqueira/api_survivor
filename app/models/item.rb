class Item < ApplicationRecord

  has_many :survivors, through: :inventories
  has_many :inventories
end
