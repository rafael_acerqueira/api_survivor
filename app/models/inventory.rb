class Inventory < ApplicationRecord
  belongs_to :item
  belongs_to :survivor

  def total_points
    item.points * quantity
  end
end
