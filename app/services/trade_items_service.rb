class TradeItemsService

  attr_reader :inventory_list_one, :inventory_list_two

  def initialize inventory_list_one, inventory_list_two
    @inventory_list_one = inventory_list_one
    @inventory_list_two = inventory_list_two
  end

  def call

    inventory_list_one.each do |inventory_line_one|
      not_found = true
      quantity_one = inventory_line_one[:quantity].to_i
      inventory_one = Inventory.find inventory_line_one[:inventory_id]

      if inventory_one.quantity >= quantity_one
        inventory_list_two.each do |inventory_line_two|
          inventory_two = Inventory.find inventory_line_two[:inventory_id]
          quantity_two = inventory_line_two[:quantity].to_i
          if inventory_two.quantity >= quantity_two
            if inventory_one.item == inventory_two.item
              not_found = false
              inventory_one.update(quantity: (inventory_one.quantity - quantity_one + quantity_two))
              inventory_two.update(quantity: (inventory_two.quantity + quantity_one - quantity_two))
            end
          end
        end
        if not_found
          inventory_two = Inventory.find inventory_list_two.first[:inventory_id]
          inventory_one.update(quantity: inventory_one.quantity - quantity_one)
          inventory_two.survivor.inventories.create(quantity: quantity_one, item: inventory_one.item)
        end
      end
    end
    
    inventory_list_two.each do |inventory_line_two|
      not_found = true
      quantity_two = inventory_line_two[:quantity].to_i
      inventory_two = Inventory.find inventory_line_two[:inventory_id]
      if inventory_two.quantity >= quantity_two
        inventory_list_one.each do |inventory_line_one|
          inventory_one = Inventory.find inventory_line_one[:inventory_id]
          if inventory_one.item == inventory_two.item
            not_found = false
          end
        end
        if not_found
          inventory_one = Inventory.find inventory_list_one.first[:inventory_id]
          inventory_two.update(quantity: inventory_two.quantity - quantity_two)
          inventory_one.survivor.inventories.create(quantity: quantity_two, item: inventory_two.item)
        end
      end
    end
  end
end
