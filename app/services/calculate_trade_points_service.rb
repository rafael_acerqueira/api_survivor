class CalculateTradePointsService

  attr_reader :inventories

  def initialize inventories
    @inventories = inventories
  end

  def call
    points = 0

    inventories.each do |trade_item|
      
      inventory = Inventory.find trade_item[:inventory_id]
      points = points + trade_item[:quantity].to_i * inventory.item.points
    end
    points
  end
end
