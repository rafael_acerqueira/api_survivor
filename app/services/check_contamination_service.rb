class CheckContaminationService

  attr_reader :inventory_list

  def initialize inventory_list
    @inventory_list = inventory_list
  end

  def call
    inventory = Inventory.find inventory_list.first[:inventory_id]
    survivor = inventory.survivor
    survivor.contamined?    
  end
end
