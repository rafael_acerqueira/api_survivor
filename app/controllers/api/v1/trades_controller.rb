class Api::V1::TradesController < Api::V1::ApiController

  def create
    if params[:trade][:inventory_list_survivor_one] && params[:trade][:inventory_list_survivor_two]
      contamination_survivor_one = CheckContaminationService.new(params[:trade][:inventory_list_survivor_one])
      contamination_survivor_two = CheckContaminationService.new(params[:trade][:inventory_list_survivor_two])
      if !contamination_survivor_one.call && !contamination_survivor_two.call
        calculate_inventory_one_points = CalculateTradePointsService.new(params[:trade][:inventory_list_survivor_one])
        inventory_one_points = calculate_inventory_one_points.call
        calculate_inventory_two_points = CalculateTradePointsService.new(params[:trade][:inventory_list_survivor_two])
        inventory_two_points = calculate_inventory_two_points.call

        if inventory_one_points == inventory_two_points
          trade_service = TradeItemsService.new(params[:trade][:inventory_list_survivor_one], params[:trade][:inventory_list_survivor_two])
          trade_service.call
        else
          json_response(nil, :not_found)
        end
      else
        json_response(nil, :not_found)
      end
    end
  end
end
