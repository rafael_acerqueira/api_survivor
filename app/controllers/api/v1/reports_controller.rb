class Api::V1::ReportsController < Api::V1::ApiController

  def infected_survivor_percentage
    survivors = Survivor.all
    survivors_contamined = survivors.select { |survivor| survivor.contamined? }
    result = (survivors_contamined.count.to_f / survivors.count.to_f) * 100
    result = 0 if !survivors_contamined.present? && !survivors.present?
    formated_result = "#{'%.0f' % result}% of survivors are infected"
    json_response(formated_result, :found)
  end

  def non_infected_survivor_percentage
    survivors = Survivor.all
    survivors_contamined = survivors.select { |survivor| survivor.contamined? }
    result = (1.to_f - (survivors_contamined.count.to_f / survivors.count.to_f)) * 100
    result = 0 if !survivors_contamined.present? && !survivors.present?
    formated_result = "#{'%.0f' % result}% of survivors are infected"
    json_response(formated_result, :found)
  end

  def average_item_by_survivor
    item = Item.find params[:item_id]
    survivors_non_contamined = Survivor.select{ |survivor| !survivor.contamined? }
    survivor_items = survivors_non_contamined.inject(0) do |sum, survivor|
      sum + survivor.inventories.where(item_id: item).inject(0) { |sum, inventory| sum + inventory.quantity }
    end    
    result = survivor_items.to_f/survivors_non_contamined.count.to_f
    result = 0 if !survivor_items.present? && !survivors_non_contamined.present?
    formated_result = "#{'%.0f' % result} #{item.name} per survivor"
    json_response(formated_result, :found)
  end

  def infected_points_lost
    survivors_contamined = Survivor.select { |survivor| survivor.contamined? }
    result = survivors_contamined.inject(0) { |sum, survivor| sum + survivor.total_points }
    formated_result = "#{'%.0f' % result} points were lost"
    json_response(formated_result, :found)
  end
end
