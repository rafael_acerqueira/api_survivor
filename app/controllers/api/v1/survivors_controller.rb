class Api::V1::SurvivorsController < Api::V1::ApiController

  before_action :set_survivor, only: :update

  def create
    survivor = Survivor.create!(survivor_params)
    json_response(survivor, :created)
  end

  def update
    @survivor.update(survivor_params)
    json_response(@survivor, :no_content)
  end


  private
  def survivor_params
    params.require(:survivor).permit(:name, :age, :gender, :latitude, :longitude)
  end

  def set_survivor
    @survivor = Survivor.find(params[:id])
  end
end
