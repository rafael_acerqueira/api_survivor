class Api::V1::ContaminationsController < Api::V1::ApiController

  def create
    contamination = Contamination.create!(contamination_params)
    json_response(contamination, :created)
  end

  private
  def contamination_params
    params.require(:contamination).permit(:survivor_id, :contamined_survivor_id)
  end
end
